train = importdata('learn_data.txt')/1000000;
clusters = importdata('clusters.txt');
test = importdata('test_data.txt')/1000000;

genOpt = genfisOptions('SubtractiveClustering', 'ClusterInfluenceRange',[0.1 0.1 0.3]);
initFis = genfis(train, clusters, genOpt)

opt = anfisOptions('InitialFIS', initFis, 'EpochNumber',450);
fis = anfis([train, clusters], opt);
result = round(evalfis(test, fis));
save('anfis_clusters.txt', 'result', '-ascii');
