import numpy as np
import matplotlib.pyplot as plt
import keras
import SimpSOM as sps
import skfuzzy
# Skrypt wizualizujący wyniki klastrowania poszczególnych metod K-means, NueralNetwork(keras model) oraz
# SOM na testowym zbiorze danych.
# Dodano ANFIS, którego model jest w matlabie, tutaj tylko wizualizacja wyników.


def draw2d(data, clusters, titles):
    colors = ["red", "green", "blue", "yellow", "turquoise", "chocolate", "hotpink", "slategray", "orange",
              "purple", "navy", "khaki", "saddlebrown", "lime", "bisque"]
    fig, axs = plt.subplots(5, 1, constrained_layout=True, figsize=(5, 9))
    for a in range(len(axs)):
        for i in range(len(colors)):
            axs[a].scatter(data[np.where(clusters[a] == i), 0], data[np.where(clusters[a] == i), 1], c=colors[i])
        axs[a].set_title(titles[a])


def draw3d(data, clusters, x, y, z, titles):
    fig = plt.figure(figsize=plt.figaspect(0.25))
    ax1 = fig.add_subplot(1, 4, 1, projection='3d')
    ax2 = fig.add_subplot(1, 4, 2, projection='3d')
    ax3 = fig.add_subplot(1, 4, 3, projection='3d')
    ax4 = fig.add_subplot(1, 4, 4, projection='3d')
    axs = [ax1, ax2, ax3, ax4]
    colors = ["blue", "yellow"]
    for a in range(len(axs)):
        for i in range(len(colors)):
            axs[a].scatter(data[np.where(clusters[a] == i), x], data[np.where(clusters[a] == i), y],
                           data[np.where(clusters[a] == i), z], c=colors[i])
        axs[a].set_title(titles[a])


def k_means(data):
    centres = np.array([[244437, 851442], [419854, 791208], [674000, 876055], [833550, 747416], [858622, 534839],
                        [139000, 560000], [339760, 564304], [605105, 572970], [166937, 345921], [395622, 401383],
                        [615579, 392717], [800622, 312990], [311829, 163935], [516075, 181267], [856484, 153536]])
    for _ in range(10):
        clusters = []
        for point in data:
            min_idx = int(np.argmin((centres[:, 0] - point[0]) ** 2 + (centres[:, 1] - point[1]) ** 2))
            clusters.append(min_idx)

        clusters_arr = np.array(clusters)
        for i in range(len(centres)):
            centres[i] = np.mean(data[np.where(clusters_arr == i)], axis=0)

    return clusters_arr


def breast(data):
    random_indices = np.random.choice(data.shape[0], size=2, replace=False)
    centres = data[random_indices, :]
    for _ in range(10):
        clusters = []
        for point in data:
            min_idx = int(np.argmin((centres[:, 0] - point[0]) ** 2 + (centres[:, 1] - point[1]) ** 2 +
                                    (centres[:, 2] - point[2]) ** 2 + (centres[:, 3] - point[3]) ** 2 +
                                    (centres[:, 4] - point[4]) ** 2 + (centres[:, 5] - point[5]) ** 2 +
                                    (centres[:, 6] - point[6]) ** 2 + (centres[:, 7] - point[7]) ** 2 +
                                    (centres[:, 8] - point[8]) ** 2))
            clusters.append(min_idx)

        clusters_arr = np.array(clusters)
        for i in range(len(centres)):
            centres[i] = np.mean(data[np.where(clusters_arr == i)], axis=0)

    return clusters_arr


def main():
    #2D
    # sieć neuronowa
    test_data = np.loadtxt("test_data.txt")
    norm_data = test_data / 1000000
    model = keras.models.load_model('model_S1.h5')
    predictions = np.argmax(model.predict(norm_data), axis=-1).flatten()

    # metoda K-means
    clusters_arr = k_means(test_data)

    # SOM
    np_load_old = np.load
    np.load = lambda *a, **k: np_load_old(*a, allow_pickle=True, **k)
    net = sps.somNet(15, 1, norm_data, 'S1_SOM.npy')
    np.load = np_load_old
    prj = np.array(net.project(norm_data, printout=False))
    k, som_clusters = np.unique(prj, axis=0, return_inverse=True)

    # fuzzy
    centres = np.loadtxt('S1_fuzzy_centres.txt')
    u, _, _, _, _, _ = skfuzzy.cluster.cmeans_predict(np.transpose(test_data), centres,
                                                      2, error=0.001, maxiter=1000)
    fuzzy_clusters = np.argmax(u, axis=0)

    # ANFIS
    anfis_clusters = np.loadtxt("anfis_clusters.txt")

    # wizaulizcja - porównanie metod
    draw2d(test_data, [predictions, clusters_arr, som_clusters, fuzzy_clusters, anfis_clusters],
           ["Keras model", "K-means", "SOM", "Fuzzy model", "ANFIS"])

    #9D
    # sieć neuronowa
    test_data = np.loadtxt("breast_test_data.txt")
    norm_data = test_data / 10
    model = keras.models.load_model('model_breast.h5')
    predictions = np.argmax(model.predict(norm_data), axis=-1).flatten()

    # metoda K-means
    clusters_arr = breast(test_data)

    # SOM
    np_load_old = np.load
    np.load = lambda *a, **k: np_load_old(*a, allow_pickle=True, **k)
    net = sps.somNet(2, 1, test_data, 'breast_SOM.npy')
    np.load = np_load_old
    prj = np.array(net.project(test_data, printout=False))
    _, som_clusters = np.unique(prj, axis=0, return_inverse=True)

    # fuzzy
    centres = np.loadtxt('breast_fuzzy_centres.txt')
    u, _, _, _, _, _ = skfuzzy.cluster.cmeans_predict(np.transpose(test_data), centres,
                                                      2, error=0.001, maxiter=1000)
    fuzzy_clusters = np.argmax(u, axis=0)

    # wizaulizcja - porównanie metod
    draw3d(test_data, [predictions, clusters_arr, som_clusters, fuzzy_clusters],  0, 1, 2,
           ['1st dimension Keras model', '1st dimension K-means', '1st dimension SOM', '1st dimension Fuzzy model'])
    draw3d(test_data, [predictions, clusters_arr, som_clusters, fuzzy_clusters],  3, 4, 5,
           ['2nd dimension Keras model', '2nd dimension K-means', '2nd dimension SOM', '2nd dimension Fuzzy model'])
    draw3d(test_data, [predictions, clusters_arr, som_clusters, fuzzy_clusters],  6, 7, 8,
           ['3rd dimension Keras model', '3rd dimension K-means', '3rd dimension SOM', '3rd dimension Fuzzy model'])
    plt.show()


if __name__ == "__main__":
    main()
