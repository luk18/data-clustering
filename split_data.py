import numpy as np

# Skrypt dzielący dane na dane do trenowania sieci (80% datasetu) oraz dane testujące wyuczoną sieć (20% datasetu).
# Dane weryfikacyjne są umieszczone w części do trenowania, w trakcie nauki na walidacje brane jest 15% z częsci
# do trenowania.

def data_split(data):
    rnd_idx = np.random.choice(data.shape[0], size=int(0.8 * data.shape[0]), replace=False)
    learn_data = data[rnd_idx, :]
    test_data = np.delete(data, rnd_idx, axis=0)
    return learn_data, test_data


def main():
    data = np.loadtxt("s1.txt")
    learn_data, test_data = data_split(data)
    np.savetxt('test_data.txt', test_data, fmt='%i')
    np.savetxt('learn_data.txt', learn_data, fmt='%i')
    data = np.loadtxt("breast.txt")
    breast_learn_data, breast_test_data = data_split(data)
    np.savetxt('breast_test_data.txt', breast_test_data, fmt='%i')
    np.savetxt('breast_learn_data.txt', breast_learn_data, fmt='%i')


if __name__ == "__main__":
    main()
