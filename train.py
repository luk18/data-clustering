import numpy as np
import keras
import SimpSOM as sps
import skfuzzy
# Skrypt trenowania sieci na podstawie zbioru danych uczących. Sieć neuronowa(model keras) uczona z nadzorem.
# Dodano trenowanie modelu rozmytego klasteryzującego na podstawie zbioru danych uczących.


def k_means(data):
    centres = np.array([[244437, 851442], [419854, 791208], [674000, 876055], [833550, 747416], [858622, 534839],
                        [139000, 560000], [339760, 564304], [605105, 572970], [166937, 345921], [395622, 401383],
                        [615579, 392717], [800622, 312990], [311829, 163935], [516075, 181267], [856484, 153536]])
    for _ in range(10):
        clusters = []
        for point in data:
            min_idx = int(np.argmin((centres[:, 0] - point[0]) ** 2 + (centres[:, 1] - point[1]) ** 2))
            clusters.append(min_idx)

        clusters_arr = np.array(clusters)
        for i in range(len(centres)):
            centres[i] = np.mean(data[np.where(clusters_arr == i)], axis=0)

    return clusters_arr


def breast(data):
    random_indices = np.random.choice(data.shape[0], size=2, replace=False)
    centres = data[random_indices, :]
    for _ in range(10):
        clusters = []
        for point in data:
            min_idx = int(np.argmin((centres[:, 0] - point[0]) ** 2 + (centres[:, 1] - point[1]) ** 2 +
                                    (centres[:, 2] - point[2]) ** 2 + (centres[:, 3] - point[3]) ** 2 +
                                    (centres[:, 4] - point[4]) ** 2 + (centres[:, 5] - point[5]) ** 2 +
                                    (centres[:, 6] - point[6]) ** 2 + (centres[:, 7] - point[7]) ** 2 +
                                    (centres[:, 8] - point[8]) ** 2))
            clusters.append(min_idx)

        clusters_arr = np.array(clusters)
        for i in range(len(centres)):
            centres[i] = np.mean(data[np.where(clusters_arr == i)], axis=0)

    return clusters_arr


def keras_model(input_size, output_size):
    model = keras.Sequential()
    model.add(keras.layers.Dense(32, input_dim=input_size, activation='relu'))
    model.add(keras.layers.Dense(16, activation='relu'))
    model.add(keras.layers.Dense(output_size, activation='sigmoid'))
    opt = keras.optimizers.Adam(lr=0.001)
    model.compile(loss='binary_crossentropy', optimizer=opt, metrics=['mae'])
    return model


def train_fuzzy(data, num_of_clusters):
    centres, _, _, _, _, _, _ = skfuzzy.cluster.cmeans(data, num_of_clusters,
                                                       2, error=0.001, maxiter=1000)
    return centres


def main():
    #2D
    learn_data = np.loadtxt("learn_data.txt")
    clusters_arr = k_means(learn_data)
    output = np.array([np.eye(15)[i] for i in clusters_arr])
    input = learn_data / 1000000
    # sieć neuronowa
    model = keras_model(2, 15)
    model.fit(input, output, validation_split=0.15, epochs=200, batch_size=20)
    keras.models.save_model(model, 'model_S1.h5')
    # SOM
    net = sps.somNet(15, 1, input, PBC=True, PCI=True)
    net.train(0.1, 20000)
    net.save('S1_SOM')
    # fuzzy
    centres = train_fuzzy(np.transpose(learn_data), 15)
    np.savetxt("S1_fuzzy_centres.txt", centres)

    #9D
    learn_data = np.loadtxt("breast_learn_data.txt")
    clusters_arr = breast(learn_data)
    output = np.array([np.eye(2)[i] for i in clusters_arr])
    input = learn_data
    # sieć neuronowa
    model = keras_model(9, 2)
    model.fit(input, output, validation_split=0.15, epochs=200, batch_size=20)
    keras.models.save_model(model, 'model_breast.h5')
    # SOM
    net = sps.somNet(2, 1, learn_data, PBC=True, PCI=True)
    net.train(0.1, 20000)
    net.save('breast_SOM')
    # fuzzy
    centres = train_fuzzy(np.transpose(learn_data), 2)
    np.savetxt("breast_fuzzy_centres.txt", centres)


if __name__ == "__main__":
    main()
